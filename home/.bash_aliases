alias ll='ls -alh'
alias x='exit'
alias wall='exec /usr/bin/feh -z --bg-fill /home/joe/.wallpapers/* & '
#alias off='sudo shutdown -P now'
alias off='systemctl poweroff'
alias reboot='systemctl reboot'
alias reset='systemctl reboot'
alias up='sudo pacman -Syu'
alias suspend='systemctl suspend'

# Can remove the "?format=x" for full report. x=1 to 4.
alias full-weather='curl wttr.in/Chinchilla'
alias weather='curl wttr.in/Chinchilla?format=4'

alias usboff='udisksctl power-off -b /dev/sdb'
alias fix='xrandr -s 1600x900 --output VGA1 --off'

alias lookup='sdcv -en'
alias word='sdcv -en'
alias search='sdcv -en'
alias look='sdcv -en'
alias s='sdcv -en'
# alias emacs='emacsclient -c'

alias xma='xmahjongg --tileset real --bg w'
alias fix2="xrandr --auto --output VGA1 --mode 1920x1080 --right-of LVDS1 && /home/joe/.fehbg"
